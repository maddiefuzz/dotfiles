set nocompatible
set showmatch
set ignorecase
set hlsearch
set incsearch
set tabstop=2
set softtabstop=2
set expandtab
set shiftwidth=2
set autoindent
set number
set wildmode=longest,list
set cc=100
filetype plugin indent on
syntax on
set mouse=a
set clipboard=unnamedplus
filetype plugin on
set cursorline
set ttyfast


call plug#begin()
" Plugin section
    Plug 'simrat39/rust-tools.nvim'
    Plug 'dracula/vim'
    Plug 'ryanoasis/vim-devicons'
    Plug 'nvim-tree/nvim-tree.lua'
    Plug 'nvim-tree/nvim-web-devicons'
    Plug 'mhinz/vim-startify'
call plug#end()

colorscheme dracula

lua << EOF
vim.keymap.set('n', '<c-t>', ':NvimTreeToggle<CR>', {
  noremap = true
})
EOF

lua require'nvim-tree'.setup {}
